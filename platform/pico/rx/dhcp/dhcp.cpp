#include "dhcp.h"

#include <cassert>
#include <cstring>
#include <iostream>
#include <lwip/netif.h>
#include <lwip/pbuf.h>
#include <lwip/udp.h>

namespace fiwi { namespace pico { namespace rx {

#define DHCP_OPT_PAD                (0)
#define DHCP_OPT_SUBNET_MASK        (1)
#define DHCP_OPT_ROUTER             (3)
#define DHCP_OPT_DNS                (6)
#define DHCP_OPT_HOST_NAME          (12)
#define DHCP_OPT_REQUESTED_IP       (50)
#define DHCP_OPT_IP_LEASE_TIME      (51)
#define DHCP_OPT_MSG_TYPE           (53)
#define DHCP_OPT_SERVER_ID          (54)
#define DHCP_OPT_PARAM_REQUEST_LIST (55)
#define DHCP_OPT_MAX_MSG_SIZE       (57)
#define DHCP_OPT_VENDOR_CLASS_ID    (60)
#define DHCP_OPT_CLIENT_ID          (61)
#define DHCP_OPT_END                (255)


namespace {
constexpr uint16_t dhcp_server_port	= 67;
constexpr uint16_t dhcp_client_port	= 68;
constexpr uint32_t dhcp_lease_time	= 24 * 60 * 60; // seconds
constexpr uint32_t dhcp_min_size	= 240 + 3;
enum dhcp_op_t {
	discover	= 1,
	offer		= 2,
	request		= 3,
	decline		= 4,
	ack			= 5,
	nack		= 6,
	release		= 7,
	inform		= 8,
};
enum dhcp_opt_t {
	pad					= 0,
	subnet_mask			= 1,
	router				= 3,
	dns					= 6,
	host_name			= 12,
	requested_ip		= 50,
	ip_lease_time		= 51,
	msg_type			= 53,
	server_id			= 54,
	param_reuest_list	= 55,
	max_msg_size		= 57,
	vendor_class_id		= 60,
	client_id			= 61,
	end					= 255,
};
struct dhcp_msg_t {
    uint8_t op;				// message opcode
    uint8_t htype;			// hardware address type
    uint8_t hlen;			// hardware address length
    uint8_t hops;
    uint32_t xid;			// transaction id, chosen by client
    uint16_t secs;			// client seconds elapsed
    uint16_t flags;
    uint8_t ciaddr[4];		// client IP address
    uint8_t yiaddr[4];		// your IP address
    uint8_t siaddr[4];		// next server IP address
    uint8_t giaddr[4];		// relay agent IP address
    uint8_t chaddr[16];		// client hardware address
    uint8_t sname[64];		// server host name
    uint8_t file[128];		// boot file name
    uint8_t options[312];	// optional parameters, variable, starts with magic
};
struct auto_release {
	explicit auto_release(uint32_t len) :
		buf(pbuf_alloc(PBUF_TRANSPORT, len, PBUF_RAM)) { assert(buf); }
	auto_release(void *src, uint32_t len) : auto_release(len) {
		std::memcpy(buf->payload, src, buf->len);
	}
	explicit auto_release(pbuf *p) : buf(p) { assert(buf); }
	~auto_release() { pbuf_free(buf); }
	pbuf *operator->() const { return buf; }
	operator pbuf*() const { return buf; }
	pbuf *buf;
};
uint8_t *find_option(uint8_t *opts, uint8_t opt) {
	for (int i = 0; i < 308 && opts[i] != dhcp_opt_t::end;) {
		if (opts[i] == opt)
			return &opts[i];
		i += 2 + opts[i + 1];
	}
    return nullptr;
}
void opt_write(uint8_t **opt, uint8_t cmd, uint8_t val) {
	uint8_t *o = *opt;
	*o++ = cmd;
	*o++ = 1;
	*o++ = val;
	*opt = o;
}
void opt_write(uint8_t **opt, uint8_t cmd, uint32_t val) {
	uint8_t *o = *opt;
	*o++ = cmd;
	*o++ = 4;
	*o++ = val;
	*o++ = val >> 8;
	*o++ = val >> 16;
	*o++ = val >> 24;
	*opt = o;
}
}

void DHCP::dhcp_recv_callback(void *arg, udp_pcb *pcb, pbuf *p,
							  const ip_addr_t *src, uint16_t port) {
	DHCP *dhcp = static_cast<DHCP*>(arg);
	dhcp->request(p, src, port);
}

DHCP::DHCP() {
	net_if = netif_default;
	udp = udp_new();
	udp_recv(udp, dhcp_recv_callback, this);
	udp_bind(udp, &net_if->ip_addr, dhcp_server_port);
}

DHCP::~DHCP() {
	if (udp)
		udp_remove(udp);
}

void DHCP::request(pbuf *buf, const ip_addr_t *src, uint16_t port) {
	auto_release p(buf);
	if (p->tot_len < dhcp_min_size)
		return;

	dhcp_msg_t msg;

	size_t len = pbuf_copy_partial(p, &msg, sizeof(msg), 0);
	if (len < dhcp_min_size)
		return;

	msg.op = dhcp_op_t::offer;
	std::memcpy(&msg.yiaddr, &ip4_addr_get_u32(ip_2_ip4(&net_if->ip_addr)), 4);

	uint8_t *opt = static_cast<uint8_t*>(msg.options);
	opt += 4; // skip magic
	uint8_t *msg_type = find_option(opt, dhcp_opt_t::msg_type);
	if (!msg_type)
		return;

		switch(msg_type[2]) {
		// TODO: handle expiry
		case dhcp_op_t::discover: {
			if (std::memcmp(&lease.mac_addr, "\0\0\0\0\0\0",
				sizeof(lease.mac_addr)) == 0) {
				msg.yiaddr[3] = 16;
				opt_write(&opt, dhcp_opt_t::msg_type,
						  uint8_t(dhcp_op_t::offer));
			}
			break;
		}
		case dhcp_op_t::request: {
			uint8_t *o = find_option(opt, dhcp_opt_t::requested_ip);
			if (o == nullptr)
				return;
			memcpy(&lease.mac_addr, msg.chaddr, 6);
			msg.yiaddr[3] = 16;
			opt_write(&opt, dhcp_opt_t::msg_type, uint8_t(dhcp_op_t::ack));
			break;
		}
		default:
			return;
	}
	opt_write(&opt, dhcp_opt_t::server_id,
			  ip4_addr_get_u32(&ip_2_ip4(net_if->ip_addr)));
	opt_write(&opt, dhcp_opt_t::subnet_mask,
			  ip4_addr_get_u32(&ip_2_ip4(net_if->netmask)));
	opt_write(&opt, dhcp_opt_t::ip_lease_time, PP_HTONL(dhcp_lease_time));
	*opt++ = dhcp_opt_t::end;

	reply(&msg, opt - reinterpret_cast<uint8_t*>(&msg));
}

void DHCP::reply(void *buf, uint32_t len) {
	ip_addr_t dst = IPADDR4_INIT(0xffffffff);
	auto_release p(buf, len);
	udp_sendto_if(udp, p, &dst, dhcp_client_port, net_if);
}

}}} // namespace fiwi::pico::rx
