#ifndef __RX_DHCP_H__
#define __RX_DHCP_H__

#include <lwip/ip_addr.h>

struct pbuf;
struct netif;
struct udp_pcb;

namespace fiwi { namespace pico { namespace rx {

class DHCP {
public:
	DHCP();
	~DHCP();
private:
	static void dhcp_recv_callback(void *arg, udp_pcb *pcb, pbuf *p,
								   const ip_addr_t *src, uint16_t port);

	void request(pbuf *buf, const ip_addr_t *src, uint16_t port);
	void reply(void *buf, uint32_t len);

	struct {
		ip_addr_t ip_addr;
		uint8_t mac_addr[6];
		uint16_t expiry;
	} lease = {};
	netif *net_if = nullptr;
	udp_pcb *udp = nullptr;
};

}}} // namespace fiwi::pico::rx

#endif //__RX_DHCP_H__