#ifndef __RX_LED_H__
#define __RX_LED_H__

namespace fiwi { namespace pico { namespace rx {

namespace Led {
	void on();
	void off();
	void toggle();
	bool is_on();
}

}}} // namespace fiwi::pico::rx

#endif //__RX_LED_H__