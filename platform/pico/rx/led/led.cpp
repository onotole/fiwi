#include "led.h"

#include <pico/cyw43_arch.h>

namespace fiwi { namespace pico { namespace rx {

namespace {
bool led_on = false;
}

void Led::on() {
	cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 1);
	led_on = true;
}

void Led::off() {
	cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 0);
	led_on = false;
}

void Led::toggle() {
	if (is_on())
		off();
	else
		on();
}

bool Led::is_on() {
	return led_on;
}

}}}