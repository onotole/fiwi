#include "config.h"

#include <ap/ap.h>
#include <dhcp/dhcp.h>
#include <led/led.h>

#include <iomanip>
#include <iostream>

#include <pico/stdlib.h>
#include <pico/cyw43_arch.h>
#include <lwip/netif.h>

void platform_init() {
	stdio_init_all();
	cyw43_arch_init();
}

void loop_core0() {
	while(true) {
		cyw43_arch_poll();
		cyw43_arch_wait_for_work_until(make_timeout_time_ms(1000));
		fiwi::pico::rx::Led::toggle();
	}
}

int main() {
	platform_init();

	std::string ssid = FIWI_RX_AP_SSID_PREFIX;
	std::string passwd = FIWI_RX_AP_PASSWORD;
	fiwi::pico::rx::AP ap(ssid + "pico", passwd);

	fiwi::pico::rx::DHCP dhcp;

	loop_core0();

	return 0;
}