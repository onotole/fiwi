#include "ap.h"

#include <pico/cyw43_arch.h>

namespace fiwi { namespace pico { namespace rx {

AP::AP(const std::string &ssid, const std::string& passwd) {
	cyw43_arch_enable_ap_mode(ssid.c_str(), passwd.c_str(),
							  CYW43_AUTH_WPA2_AES_PSK);
}

AP::~AP() {
	cyw43_arch_disable_ap_mode();
}

}}} // namespace fiwi::pico::rx
