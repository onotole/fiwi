#ifndef __RX_AP_H__
#define __RX_AP_H__

#include <string>

namespace fiwi { namespace pico { namespace rx {

class AP {
public:
	AP(const std::string &ssid, const std::string& passwd);
	AP(const AP&) = delete;
	AP(AP&&) = delete;
	~AP();
};

}}} // namespace fiwi::pico::rx

#endif //__RX_AP_H__